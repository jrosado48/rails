// Create launch template to manage EC2 instances
// Assumes SSH key has already been created 
// https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/launch_template
resource "aws_launch_template" "rails_launch_template" {
  name                    = "rails_launch_template"
  description             = "Launch template for EC2 instances in ASG"
  image_id                = var.ami  // Ubuntu 18.04
  instance_type           = var.instance_type
  key_name                = var.key_name
  vpc_security_group_ids  = [ aws_security_group.ec2_sg.id ]

  iam_instance_profile {
    name = "ecs-agent"
  }

  monitoring {
    enabled = true
  }
   
}

// Create ASG and reference launch template
// https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/autoscaling_group
resource "aws_autoscaling_group" "rails_asg" {
  name = "rails_asg"
  desired_capacity    = 2
  max_size            = 4
  min_size            = 2
  target_group_arns   = [ aws_lb_target_group.lb_targetgroup.arn ]
  vpc_zone_identifier = [ aws_subnet.public_primary.id, aws_subnet.public_secondary.id ]
  health_check_type   = "ELB"
  load_balancers      = [ aws_lb.rails_lb.id ]


  launch_template {
    id      = aws_launch_template.rails_launch_template.id
    version = "$Latest"
  }
}

// Create Scaling Policy for ASG
// https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/autoscaling_policy
resource "aws_autoscaling_policy" "cpu_utilization" {
  name                   = "rails_asg_scaling_policy"
  scaling_adjustment     = 1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = aws_autoscaling_group.rails_asg.name

    target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageCPUUtilization"
    }

    target_value = 40.0
  }
}

