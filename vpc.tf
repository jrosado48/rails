# Create Rails App VPC
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc
resource "aws_vpc" "rails_vpc" {
  cidr_block = var.vpc_cidr 

  tags = {
    Name = "Rails VPC"
  }
}

# Create primary public subnet in Rails VPC
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet
resource "aws_subnet" "public_primary" {
  vpc_id     = aws_vpc.rails_vpc.id
  cidr_block = var.public_subnet_primary
  availability_zone = var.primary_az

  tags = {
    Name = "Public Subnet 1"
  }
}

# Create secondary public subnet in Rails VPC
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet
resource "aws_subnet" "public_secondary" {
  vpc_id     = aws_vpc.rails_vpc.id
  cidr_block = var.public_subnet_secondary
  availability_zone = var.secondary_az

  tags = {
    Name = "Public Subnet 1"
  }
}

# Create Private Subnet in Rails VPC
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet
resource "aws_subnet" "private" {
  vpc_id     = aws_vpc.rails_vpc.id
  cidr_block = var.private_subnet

  tags = {
    Name = "Private Subnet"
  }
}

# Create Internet Gateway for Rails VPC
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/internet_gateway
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.rails_vpc.id

  tags = {
    Name = "Rails IGW"
  }
}

# Create Elastic IP for Primary NAT Gateway
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eip
resource "aws_eip" "primary_nat_eip" {
  vpc        = true
  depends_on = [aws_internet_gateway.igw]
  tags = {
    Name = "NAT Gateway EIP"
  }
}

# Create Elastic IP for Secondary NAT Gateway
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eip
resource "aws_eip" "secondary_nat_eip" {
  vpc        = true
  depends_on = [aws_internet_gateway.igw]
  tags = {
    Name = "NAT Gateway EIP"
  }
}

# Create Primary NAT Gateway for Rails VPC
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/nat_gateway
resource "aws_nat_gateway" "nat_primary" {
  allocation_id = aws_eip.primary_nat_eip.id
  subnet_id     = aws_subnet.public_primary.id

  depends_on = [ aws_internet_gateway.igw ]

  tags = {
    Name = "Primary NAT Gateway"
  }
}

# Create Seondary NAT Gateway for Rails VPC
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/nat_gateway
resource "aws_nat_gateway" "nat_secondary" {
  allocation_id = aws_eip.secondary_nat_eip.id
  subnet_id     = aws_subnet.public_secondary.id

  depends_on = [ aws_internet_gateway.igw ]

  tags = {
    Name = "Secondary NAT Gateway"
  }
}

# Create Route Table for Primary Public Subnet
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table
resource "aws_route_table" "public_primary" {
  vpc_id = aws_vpc.rails_vpc.id

  route {
    cidr_block = var.default_route 
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = {
    Name = "Primary Public Route Table"
  }
}

# Association between Public Subnet and Public Route Table
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association
resource "aws_route_table_association" "public_primary" {
  subnet_id      = aws_subnet.public_primary.id
  route_table_id = aws_route_table.public_primary.id
}

# Create Route Table for Secondary Public Subnet
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table
resource "aws_route_table" "public_secondary" {
  vpc_id = aws_vpc.rails_vpc.id

  route {
    cidr_block = var.default_route 
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = {
    Name = "Secondary Public Route Table"
  }
}

# Association between Secondary Public Subnet and Secondary Public Route Table
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association
resource "aws_route_table_association" "public_secondary" {
  subnet_id      = aws_subnet.public_secondary.id
  route_table_id = aws_route_table.public_secondary.id
}

# Route Table for Private Subnet
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table
resource "aws_route_table" "private" {
  vpc_id = aws_vpc.rails_vpc.id

  route {
    cidr_block = var.default_route
    nat_gateway_id = aws_nat_gateway.nat_primary.id
  }

  tags = {
    Name = "Private Route Table"
  }
}

# Association between Private Subnet and Private Route Table
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association
resource "aws_route_table_association" "private" {
  subnet_id      = aws_subnet.private.id
  route_table_id = aws_route_table.private.id
}