// Create PostgreSQL DB
// https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/db_instance
resource "aws_db_instance" "postgresql_db" {
  allocated_storage         = 20
  max_allocated_storage     = 100
  storage_type              = "gp2"
  engine                    = "postgres"
  engine_version            = "12.5"
  instance_class            = "db.t2.micro"
  backup_retention_period   = 7
  vpc_security_group_ids    = [ aws_security_group.db_sg.id ]
  db_subnet_group_name      = "db_subnet"
  name                      = "rails_db" 
  username                  = var.username
  password                  = var.password
  parameter_group_name      = "default.postgres12"

}

// Create PostgreSQL DB Subnet Group
// https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/db_subnet_group
resource "aws_db_subnet_group" "db_subnet" {
  name       = "db_subnet"
  subnet_ids = [ aws_subnet.public_primary.id, aws_subnet.public_secondary.id ]

  tags = {
    Name = "Rails DB subnet group"
  }
}