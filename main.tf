// Configure AWS Provider
// https://registry.terraform.io/providers/hashicorp/aws/latest
terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "3.27.0"
    }
  }
}

provider "aws" {
  region = var.region
}

// Create terraform backend to securely store terraform state
// Assumes S3 bucket has already been created
// https://www.terraform.io/docs/language/settings/backends/s3.html
terraform {
  backend "s3" {
    bucket          = "rails-terraform-state"
    key             = "rails/terraform.tfstate"
    region          = "us-east-2"
    dynamodb_table  = "tfstate-locking"
    encrypt         = true
  }
}

// Create DB to lock tfstate when running terraform apply
// https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/dynamodb_table
resource "aws_dynamodb_table" "tf_lock" {
  name          = "tfstate-locking"
  billing_mode  = "PAY_PER_REQUEST"
  hash_key      = "LockID"

  attribute {
    name = "LockID"
    type = "S"
  }

}

