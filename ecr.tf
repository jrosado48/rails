// Create ECR Repo
// https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecr_repository
resource "aws_ecr_repository" "rails_app" {
  name                 = "rails-app-repo"
}