variable "region" {
  default = "us-east-2"
}

variable "key_name" {
  default = "super_secret_key"
}

variable "ami" {
  default = "ami-02aa7f3de34db391a"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "default_route" {
  default = "0.0.0.0/0"
}

variable "public_subnet_primary" {
  default = "10.0.0.0/24"
}

variable "public_subnet_secondary" {
  default = "10.0.1.0/24"
}

variable "private_subnet" {
  default = "10.0.2.0/24"
}

variable "vpc_cidr" {
  default = "10.0.0.0/18"
}

variable "primary_az" {
  default = "us-east-2a"
}

variable "secondary_az" {
  default = "us-east-2b"
}

// NOTE ****
// Remember to export var value in shell before terraform apply
// I.E.. export TF_VAR_username=(the username)
// This avoids hard coding secrets in plain text
variable "username" {
  description = "The username for the postgreSQL DB"
  type = string
}

// NOTE ****
// Remember to export var value in shell before terraform apply
// I.E.. export TF_VAR_password=(the password)
// This avoids hard coding secrets in plain text
variable "password" {
  description = "The password for the postgreSQL DB"
  type = string
}

