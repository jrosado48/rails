Features

- Auto Scaling Group to promote scalability
- Application Load Balancer to promote high availability
- Security Groups to promote secure access to application and resources
- Route 53 A record with health check to promote observability
- VPC with a public and private subnet
- ECS cluster to promote high availability and scalability of application
- Terraform graph to visualize dependencies between infrastructure


To view Terraform graph
- Download file 
>  https://gitlab.com/jrosado48/rails/-/blob/master/rails_app_infra.svg


PostgreSQL Secrets
- To avoid storing secrets in plain text, the username and password of the DB have instead been moved to variables.tf
- You can use environment variables to declare the values of the DB username and password
- I.E.. `export TF_VAR_username=<insert username here>`
