// Create ECS Cluster
// https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecs_cluster
resource "aws_ecs_cluster" "rails_cluster" {
  name = "rails-cluster"
}

// Create Task definition
// https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecs_task_definition
resource "aws_ecs_task_definition" "task_definition" {
  family                = "rails_app"
  container_definitions = file("task_definition.json.tpl")
}


// Create ECS Service to maintain 2 running task
// https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecs_service
resource "aws_ecs_service" "rails_app" {
  name            = "rails_app"
  cluster         = aws_ecs_cluster.rails_cluster.id
  task_definition = aws_ecs_task_definition.task_definition.arn
  desired_count   = 2
}