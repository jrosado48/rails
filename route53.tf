// Create Public Zone
// https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_zone
resource "aws_route53_zone" "primary" {
  name = "rails-app.com"
}

// Create A record for example.com and Alias record to point to Rails LB
// https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_record
resource "aws_route53_record" "www" {
  zone_id = aws_route53_zone.primary.zone_id
  name    = "www.rails-app.com"
  type    = "A"

  alias {
    name                   = aws_lb.rails_lb.dns_name
    zone_id                = aws_lb.rails_lb.zone_id
    evaluate_target_health = true
  }
}

// Create HTTPS health check for rails-app.com
// https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_health_check
resource "aws_route53_health_check" "app_health_check" {
  fqdn              = "example.com"
  port              = 443
  type              = "HTTPS"
  resource_path     = "/"
  failure_threshold = "5"
  request_interval  = "30"

  tags = {
    Name = "rails-app-health-check"
  }
}