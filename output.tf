//Output ECR url for use in task definition file
 output "ecr_repository_url" {
  value = aws_ecr_repository.rails_app.repository_url
}