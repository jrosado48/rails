// Create Application Load Balancer
// https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lb
resource "aws_lb" "rails_lb" {
  name               = "rails-lb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [ aws_security_group.lb_sg.id ]
  subnets            = [ aws_subnet.public_primary.id, aws_subnet.public_secondary.id ]

  enable_deletion_protection = true

  access_logs {
    bucket  = aws_s3_bucket.lb_logs.bucket
    prefix  = "lb_logs"
    enabled = true
  }

  tags = {
    Environment = "production"
  }
}

// Create Load Balancer Target Group
// https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lb_target_group
resource "aws_lb_target_group" "lb_targetgroup" {
  name     = "lb-targetgroup"
  port     = 443
  protocol = "HTTPS"
  vpc_id   = aws_vpc.rails_vpc.id
}

// Create Load Balancer HTTPS Listener
// Assumes SSL certificate was created using ACM
// https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lb_listener
resource "aws_lb_listener" "rails_tls" {
  load_balancer_arn = aws_lb.rails_lb.arn
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = "arn:aws:acm:us-east-2:00012345678:certificate/abcd-abcd-abcd-1234-abcd"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.lb_targetgroup.arn
  }
}